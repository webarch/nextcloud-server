#!/usr/bin/env bash
  
if [[ "${1}" != "" ]]; then
  # ansible-galaxy install -r requirements.yml --force && \
    ansible-playbook nextcloud.yml --tags "${1}" -vv
else
  # ansible-galaxy install -r requirements.yml --force && \
    ansible-playbook nextcloud.yml -vv
fi

