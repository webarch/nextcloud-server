#!/usr/bin/env bash

if [[ "${1}" != "" && "${2}" != "" ]]; then
  ansible-galaxy install -r requirements.yml --force apt ssh fail2ban ufw sudo users && \
    ansible-playbook root.yml -i hosts.yml --limit "${1}" --tags "${2}"
elif [[ "${1}" != "" ]]; then
  ansible-galaxy install -r requirements.yml --force apt ssh fail2ban ufw sudo users && \
    ansible-playbook root.yml -i hosts.yml --limit "${1}"
else
  ansible-galaxy install -r requirements.yml --force apt ssh fail2ban ufw sudo users && \
    ansible-playbook root.yml -i hosts.yml -v
fi
