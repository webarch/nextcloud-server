#!/usr/bin/env bash

set -e -o pipefail

if [[ "${1}" ]]; then
  echo "Updating role ${1}..."
  ansible-galaxy install -r requirements.yml --force "${1}"
else
  echo "Updating all roles..."
  ansible-galaxy install -r requirements.yml --force
fi
