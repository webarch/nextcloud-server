#!/usr/bin/env bash

set -e -o pipefail

echo "Updating the requirements.yml file..."
ansible-playbook req.yml || exit

