#!/usr/bin/env bash

if [[ "${2}" == "--no-check" ]]; then
  # ansible-galaxy install -r requirements.yml --force && \
    ansible-playbook bullseye.yml -i hosts.yml -vv --diff -l ${1}
elif [[ "${1}" != "" ]]; then
  ansible-galaxy install -r requirements.yml --force && \
    ansible-playbook bullseye.yml -i hosts.yml -vv --check --diff -l ${1}
else
  echo "Run this script like this to check: ./bullseye.sh example.org"
  echo "Run this script like this to update a server: ./bullseye.sh example.org --no-check"
fi

