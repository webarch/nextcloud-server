#!/usr/bin/env bash
  
if [[ "${1}" != "" ]]; then
  ansible-galaxy install -r requirements.yml --force fail2ban && \
    ansible-playbook fail2ban.yml --limit "${1}" -vv
else
  ansible-galaxy install -r requirements.yml --force fail2ban && \
    ansible-playbook fail2ban.yml -vv
fi
