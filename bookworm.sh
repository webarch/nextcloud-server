#!/usr/bin/env bash

if [[ "${1}" == "--check" ]]; then
  ansible-galaxy install -r requirements.yml --force apt bookworm upgrade && \
    ansible-playbook bookworm.yml -i hosts.yml -vv --check --diff -l wsh.webarchitects.org.uk
else
  ansible-galaxy install -r requirements.yml --force apt bookworm upgrade && \
    ansible-playbook bookworm.yml -i hosts.yml -vv --diff -l wsh.webarchitects.org.uk
fi
