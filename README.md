# Nextcloud Server

This repo was originally at
[https://git.coop/webarch/nextcloud](https://git.coop/webarch/nextcloud) and
this version can be browsed via [the `0.0.1`
tag](https://git.coop/webarch/nextcloud-server/-/tree/0.0.1#install).

The repo builds a pair of development servers for testing, one running
Nextcloud and one running ONLYOFFICE Document Server.

## ONLYOFFICE Workspace

Manually edit `/boot/grub/menu.lst` at add `vsyscall=emulate` for Docker, eg:

```
## ## End Default Options ##

title           Debian GNU/Linux, kernel 6.1.0-15-amd64
root            (hd0,0)
kernel          /boot/vmlinuz-6.1.0-15-amd64 vsyscall=emulate root=/dev/xvda2 ro elevator=noop
initrd          /boot/initrd.img-6.1.0-15-amd64

title           Debian GNU/Linux, kernel 6.1.0-15-amd64 (single-user mode)
root            (hd0,0)
kernel          /boot/vmlinuz-6.1.0-15-amd64 vsyscall=emulate root=/dev/xvda2 ro elevator=noop single
initrd          /boot/initrd.img-6.1.0-15-amd64

### END DEBIAN AUTOMAGIC KERNELS LIST
```
